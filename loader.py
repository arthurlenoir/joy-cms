from aiofile import AIOFile
from asyncio import gather, get_event_loop
from io import  StringIO
from jinja2 import BaseLoader, TemplateNotFound
from jinja2.exceptions import TemplateSyntaxError
from lxml.etree import fromstring, XMLParser, XMLSyntaxError
from os import listdir
from os.path import basename, dirname, exists, getmtime, isdir, isfile, join, split
from sanic.response import html
from sanic.router import Router
from watchgod import awatch, Change
from yaml import safe_load, YAMLError

from utils import merge


CHUNK_SIZE = 4096

parser = XMLParser(ns_clean=True)

async def read_file(filepath):
    res = StringIO()
    async with AIOFile(filepath, 'r') as afp:
        total_size = 0
        while True:
            size = res.write(await afp.read(CHUNK_SIZE, offset=total_size))
            total_size += size
            if size < CHUNK_SIZE:
                break
    return res.getvalue()

def splitall(path):
    allparts = []
    while 1:
        part, parts = split(path)
        if part == path:
            allparts.insert(0, part)
            break
        elif parts == path:
            allparts.insert(0, parts)
            break
        else:
            path = part
            allparts.insert(0, parts)
    return allparts


class Template(object):
    def __init__(self, filename, parent_directory, app, theme=None):
        self.parent_directory = parent_directory
        self.template_file = filename
        self.config_file = filename[:-4] + "yaml"
        self.app = app
        self.default_theme = theme
        self._has_changes = False

    def __deepcopy__(self, _):
        copy = Template(self.template_file, self.parent_directory, self.app, theme=self.default_theme)
        copy.template_data = self.template_data
        copy._has_changes = self._has_changes
        copy.config = self.config
        copy.settings = self.settings
        return copy

    @classmethod
    async def create(Cls, filename, parent_directory, app, **kwargs):
        self = Cls(filename, parent_directory, app, **kwargs)
        await self.load_template_file()
        await self.load_config_file()
        return self

    def set_has_changes(self):
        self._has_changes = True

    def has_not_changes(self):
        if self._has_changes:
            self._has_changes = False
            return False
        return True

    def get_filename(self):
        return self.template_file[len(self.parent_directory):]

    async def load_template_file(self):
        res = StringIO()
        async with AIOFile(self.template_file, 'r') as afp:
            total_size = 0
            while True:
                tmp = await afp.read(CHUNK_SIZE, offset=total_size)
                size = res.write(tmp)
                total_size += size
                if size < CHUNK_SIZE:
                    break
        self.template_data = res.getvalue()

    async def load_config_file(self):
        if self.has_config_file():
            res = StringIO()
            async with AIOFile(self.config_file, 'r') as afp:
                total_size = 0
                while True:
                    tmp = await afp.read(CHUNK_SIZE, offset=total_size)
                    size = res.write(tmp)
                    total_size += size
                    if size < CHUNK_SIZE:
                        break
            self.config = safe_load(res.getvalue())
            self.settings = self.config.get('settings', {})

    def has_config_file(self):
        #TODO cache this result
        return exists(self.config_file)

    def get_theme(self):
        return self.settings.get('theme', self.default_theme)

    def get_extends_directive(self):
        if 'layout' in self.settings:
            theme = self.get_theme()
            if theme is not None:
                # TODO replace with custom filter
                return "{%% extends \"themes/%s/%s\" %%} " % (theme, self.settings['layout'])
            return "{%% extends \"apps/%s/%s\" %%} " % (self.app.id, self.settings['layout'])
        return ""

    def get_source(self):
        return self.get_extends_directive() + self.template_data

    def load_template(self, env):
        return env.from_string(self.get_source())

    async def render(self, env):
        template = self.load_template(env)
        config = env.loader.get_config(self)
        return await template.render_async(**config)

    def to_dict(self, env):
        return {
            'url': self.settings.get('url'),
            'template_data': self.template_data,
            'file': self.get_filename(),
            'config': env.loader.get_config(self),
        }

    async def to_dict_with_preview(self, env):
        res = self.to_dict(env)
        try:
            res['html'] = await self.render(env)
        except TemplateSyntaxError as e:
            res['errors'] = [{
                'line': e.lineno,
                'message': e.message
            }]
        except XMLSyntaxError as e:
            res['errors'] = [{
                'line': e.lineno,
                'position': e.position,
                'offset': e.offset,
                'message': e.msg,
                'text': e.text,
                'code': e.code
            }]
        return res


def recursive_list_dir(folder):
    for filename in listdir(folder):
        filepath = join(folder, filename)
        if isdir(filepath):
            for f in recursive_list_dir(filepath):
                yield f
        else:
            yield filepath


class Theme(object):
    def __init__(self, settings_file, app, env):
        self.settings_file = settings_file
        self.theme_directory = dirname(settings_file)
        self.id = basename(self.theme_directory)
        self.app = app
        self.env = env
        self.config = {}
        self.config_files = {}
        self.templates = {}

    async def load_theme(self):
        self.config = safe_load(await read_file(self.settings_file))
        for filename in recursive_list_dir(self.theme_directory):
            if filename.endswith('.html'):
                await self.add_template(filename)

    async def add_template(self, filename):
        template = await Template.create(filename, self.theme_directory, self, theme=self.config.get('theme'))
        self.templates[filename] = template
        if template.has_config_file():
            self.config_files[template.config_file] = template
        url = template.settings.get('url')
        if url is not None:
            self.app.add_route(self.handle_route(template), url)

    def remove_template(filename):
        template = self.templates.pop(filename)
        if template.has_config_file():
            config = self.config_files.pop(template.config_file)
            url = template.settings.get('url')
            if url is not None:
                self.app.remove_route(url)

    def handle_route(self, template):
        env = self.env
        async def handle(request, **kwargs):
            print("handle", request, kwargs)
            return html(await template.render(env))
        return handle

    async def on_file_change(self, status, location):
        template = None
        if status == Change.added:
            if location.endswith('.html'):
                await self.add_template(filename)
        if status == Change.modified:
            if location.endswith('.yaml'):
                template = self.config_files.get(location)
                await template.load_config_file()
                template.set_has_changes()
            elif location.endswith('.html'):
                template = self.templates.get(location)
                await template.load_template_file()
                template.set_has_changes()
        if status == Change.deleted:
            if location.endswith('.yaml'):
                template = self.config_files.get(location)
                await template.load_config_file()
                template.set_has_changes()
            elif location.endswith('.html'):
                self.remove_template(location)

    def get_from_path(self, path):
        if path.startswith('/'):
            full_path = join(self.theme_directory, path[1:])
        else:
            full_path = join(self.theme_directory, path)
        return self.templates.get(full_path)

    def to_dict(self, env):
        return {
            'id': self.id,
            'config': self.config,
            'pages': [template.to_dict(env) for template in self.templates.values()]
        }


class TemplateLoader(BaseLoader):

    def __init__(self, themes_directory, apps_directory):
        self.themes_directory = themes_directory
        self.apps_directory = apps_directory
        self.urls = {}
        self.templates = {}
        self.config_files = {}
        self.themes = {}
        self.apps = {}
        self.app = None

    async def load_all_templates(self, app, env):
        self.app = app
        for filename in listdir(self.themes_directory):
            theme_folder = join(self.themes_directory, filename)
            if isdir(theme_folder):
                theme_file = join(theme_folder, 'theme.yaml')
                if isfile(theme_file):
                    theme = Theme(theme_file, self.app, env)
                    await theme.load_theme()
                    self.themes[theme.id] = theme
        for filename in listdir(self.apps_directory):
            app_folder = join(self.apps_directory, filename)
            if isdir(app_folder):
                app_file = join(app_folder, 'app.yaml')
                if isfile(app_file):
                    app = Theme(app_file, self.app, env)
                    await app.load_theme()
                    self.apps[app.id] = app

        loop = get_event_loop()
        loop.create_task(env.loader.watch_themes_changes())
        loop.create_task(env.loader.watch_apps_changes())

    def get_source(self, environment, template):
        if template.startswith('themes/'):
            theme_name, template = template[7:].split('/', 1)
            theme = self.themes[theme_name]
            path = join(theme.theme_directory, template)
            instance = theme.templates.get(path)
        elif template.startswith('apps/'):
            theme_name, template = template[5:].split('/', 1)
            theme = self.apps[theme_name]
            path = join(theme.theme_directory, template)
            instance = theme.templates.get(path)
        else:
            instance = None

        if instance is None:
            raise TemplateNotFound(template)
    
        return instance.get_source(), path, instance.has_not_changes

    def get_config(self, template):
        config = {}
        config.update(template.config)
        layout = template.settings.get('layout')
        app = template.app
        while layout is not None:
            theme = template.get_theme()
            if theme is not None:
                app = self.themes[theme]
            layout_path = join(app.theme_directory, layout)
            template = app.templates.get(layout_path)
            if template is None:
                raise Exception("Invalid layout parameter: layout does not exist.")
            layout = template.settings.get('layout')
            tmp = merge(template.config, {})
            config = merge(config, tmp)
        return config

    async def watch_themes_changes(self):
        async for changes in awatch(self.themes_directory):
            for status, location in changes:
                relative_path = location[len(self.themes_directory)+1:]
                theme_name = splitall(relative_path)[0]
                theme = self.themes.get(theme_name)
                if theme is None:
                    continue
                await theme.on_file_change(status, location)

    async def watch_apps_changes(self):
        async for changes in awatch(self.apps_directory):
            for status, location in changes:
                relative_path = location[len(self.apps_directory)+1:]
                app_name = splitall(relative_path)[0]
                app = self.apps.get(app_name)
                if app is None:
                    continue
                await app.on_file_change(status, location)
