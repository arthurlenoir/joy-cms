def merge(source, destination):
    for key, value in source.items():
        if isinstance(value, dict):
            if key not in destination:
                destination[key] = {}
            merge(value, destination[key])
        else:
            destination[key] = value
    return destination