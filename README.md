# Joy CMS

## Setup of the development server
### Install 
```sh
pip install -r requirements.txt
cd static
yarn install
cd ../
```

### Build JavaScript with watcher
```sh
./node_modules/.bin/rollup -w -c rollup-config.js
```

### Run Server 
```sh
python server.py
```