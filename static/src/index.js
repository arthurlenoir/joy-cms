import React, {Component, Fragment} from 'react';
import ReactDOM from 'react-dom';


import Grid from '@material-ui/core/Grid';

import Typography from '@material-ui/core/Typography';

import ListItemIcon from '@material-ui/core/ListItemIcon';
import MenuItem from '@material-ui/core/MenuItem';
import MenuList from '@material-ui/core/MenuList';
import SwipeableDrawer from '@material-ui/core/SwipeableDrawer';
import TextField from '@material-ui/core/TextField';
import { makeStyles } from '@material-ui/core/styles';

import Collections from '@material-ui/icons/Collections';
import FileCopy from '@material-ui/icons/FileCopy';

import Header from './header';
import Pages from './pages';


import '../styles/main.scss';


const useDashboardStyles = makeStyles(theme => ({
  root: {
    flexGrow: 1,
    height: "100%",
    boxSizing: "border-box",
    paddingTop: 64,
    marginLeft: theme.spacing(-1),
    marginRight: theme.spacing(-1),
  }
}));


const Media = () => {
    return <Fragment>
        <Grid item xs={2}>
            Media List
        </Grid>
        <Grid item xs={8}>
            Media View
        </Grid>
    </Fragment>;
}


const DashboardMenuItem = ({currentView, setCurrentView, Icon, id, children}) => <MenuItem 
    onClick={ () => setCurrentView(id) } 
    selected={ currentView === id }
>
    <ListItemIcon>
        <Icon />
    </ListItemIcon>
    <Typography variant="inherit">{ children }</Typography>
</MenuItem>;


const Dashboard = (props) => {
    const classes = useDashboardStyles();
    return <Grid container className={classes.root} spacing={2}>
        { props.children }
        { props.currentView === "pages" &&
            <Pages />
        ||
            <Media />
        }
    </Grid>;
}


class Main extends Component {
    constructor() {
        super();
        this.state = {
            currentView: "pages",
            menuOpen: false
        }
        this.setCurrentView = this.setCurrentView.bind(this);
        this.toggleDrawer = this.toggleDrawer.bind(this);
    }

    setCurrentView(currentView) {
        this.setState({currentView, menuOpen: false});
    }

    toggleDrawer() {
        this.setState({menuOpen: !this.state.menuOpen});
    }

    render() {
        return <Fragment>
            <Header 
                toggleDrawer={ this.toggleDrawer } 
                drawerOpen={ this.state.menuOpen }
            />
            <Dashboard 
                currentView={ this.state.currentView } 
                setCurrentView={ this.setCurrentView }
            >
                <SwipeableDrawer
                    open={ this.state.menuOpen }
                    onClose={ this.toggleDrawer }
                    onOpen={ this.toggleDrawer }
                    variant="persistent"
                    classes={{root: "main-layout-menu"}}
                >
                   <div
                      style={{width: 250}}
                      role="presentation"
                    >
                        <MenuList>
                            <DashboardMenuItem currentView={ this.state.currentView } setCurrentView={ this.setCurrentView } Icon={FileCopy} id="pages">Pages</DashboardMenuItem>
                            <DashboardMenuItem currentView={ this.state.currentView } setCurrentView={ this.setCurrentView  } Icon={Collections} id="media">Media</DashboardMenuItem>
                        </MenuList>
                    </div>
                </SwipeableDrawer>

            </Dashboard>
        </Fragment>;
    }
}

ReactDOM.render(
  <Main />,
  document.getElementById('root')
);
