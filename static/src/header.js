import React from 'react';

import AppBar from '@material-ui/core/AppBar';
import ChevronLeftIcon from '@material-ui/icons/ChevronLeft';
import IconButton from '@material-ui/core/IconButton';
import MenuIcon from '@material-ui/icons/Menu';
import Toolbar from '@material-ui/core/Toolbar';
import Typography from '@material-ui/core/Typography';

import { makeStyles } from '@material-ui/core/styles';


const useStyles = makeStyles(theme => ({
  root: {
    flexGrow: 1,
  },
  menuButton: {
    marginRight: theme.spacing(2),
  },
  title: {
    flexGrow: 1,
  },
}));

const Header = ({toggleDrawer, drawerOpen}) => {
    const classes = useStyles();
    return <AppBar position="fixed">
        <Toolbar>
            <IconButton edge="start" className={classes.menuButton} color="inherit" aria-label="Menu" onClick={ toggleDrawer }>
                { 
                    drawerOpen && 
                        <ChevronLeftIcon />
                    ||
                        <MenuIcon />
                }
            </IconButton>
            <Typography variant="h6" className={classes.title}>CMS Admin</Typography>
            <div>logout</div>
        </Toolbar>
    </AppBar>;
}

export default Header;