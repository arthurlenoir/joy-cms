import React, {Component, Fragment} from 'react';

import TextField from '@material-ui/core/TextField';


class ConfigTextField extends Component {
    constructor() {
        super();
        this.onChange = this.onChange.bind(this);
    }

    onChange(event) {
        if (typeof this.props.onChange === "function") {
            this.props.onChange({[this.props.configKey]: {[this.props.fieldKey]: event.currentTarget.value}});
        }
    }

    render() {
        const {configKey, fieldKey, ...otherProps} = this.props;
        return <TextField
            { ...otherProps }
            id={`field_${ configKey }_${ fieldKey }`}
            className="settings-field"
            margin="normal"
            onChange={ this.onChange }
        />;
    }
}

class ConfigField extends Component {
    constructor() {
        super();
        this.renderField = this.renderField.bind(this);
    }

    renderField(key) {
        return <ConfigTextField 
            required
            configKey={ this.props.configKey }
            fieldKey={ key }
            key={`field_${ this.props.configKey }_${ key }`}
            value={ this.props.changes[key] || this.props.config[key] || "" }
            label={ key }
            onChange={ this.props.onChange }
            disabled={ this.props.disabled }
        />;
    }

    render() {
        return <Fragment>{
            Object.keys(this.props.config).map(this.renderField)
        }</Fragment>;
    }
}

export default ConfigField;