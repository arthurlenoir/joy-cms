import React, {Component} from 'react';

import CodeMirror from 'react-codemirror';

import jinja2 from 'codemirror/mode/jinja2/jinja2';
//import js from 'codemirror/mode/javascript/javascript';
//import xml from 'codemirror/mode/xml/xml';
//import md from 'codemirror/mode/markdown/markdown';
import htmlmixed from 'codemirror/mode/htmlmixed/htmlmixed';

import "codemirror/lib/codemirror.css";

const options = {
    lineNumbers: true,
    mode: "htmlmixed",
    viewportMargin: Infinity
};

class Editor extends Component {

    constructor() {
        super();
        this.setRef = this.setRef.bind(this);
        this.marksError = this.marksError.bind(this);
        this.marks = [];
    }

    setRef(ref) {
        if (ref && typeof ref.getCodeMirror === "function") {
            this.codeMirror = ref.getCodeMirror();
        }
        else {
            this.codeMirror = null;
        }
    }

    marksError(error) {
        console.log("marksError", {line: error.line-1, ch: 0}, {line: error.line, ch: 0}, {
            className: 'syntax-error', 
            title: error.message
        });
        this.marks.push(this.codeMirror.markText({line: error.line-1, ch: 0}, {line: error.line, ch: 0}, {
            className: 'syntax-error', 
            title: error.message
        }));
    }

    shouldComponentUpdate(nextProps, nextState) {
        if (nextProps.errors !== this.props.errors && this.codeMirror) {
            this.marks.forEach(marker => marker.clear());
            nextProps.errors && nextProps.errors.map(this.marksError);
        }
        return nextProps.value !== this.props.value || nextProps.errors !== this.props.errors || nextProps.disabled !== this.props.disabled;
    }

    render() {
        return <CodeMirror 
            value={this.props.value} 
            onChange={this.props.onChange} 
            className={this.props.disabled && "disabled" || ""}
            options={{
                ...options,
                readOnly: this.props.disabled
            }}
            ref={ this.setRef } 
        />;
    }
}

export default Editor;