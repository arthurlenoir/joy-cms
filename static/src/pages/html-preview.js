import React, {Component} from 'react';

class HTMLPreview extends Component {
    constructor() {
        super();
        this.setRef = this.setRef.bind(this);
    }

    setRef(ref) {
        this.ref = ref;
        if (ref) {
            this.writeHTML(this.props.html);
        }
    }

    writeHTML(html) {
        this.ref.contentWindow.document.open();
        this.ref.contentWindow.document.clear();
        this.ref.contentWindow.document.write(html);
        this.ref.contentWindow.document.close();
    }

    shouldComponentUpdate(nextProps) {
        if (nextProps.html !== this.props.html && this.ref) {
            this.writeHTML(nextProps.html);
        }
        return false;
    }

    render() {
        return <iframe 
            ref={ this.setRef }
            className="ReactCodeMirrorPreview"
        ></iframe>;
    }
}

export default HTMLPreview;