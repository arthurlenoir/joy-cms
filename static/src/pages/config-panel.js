import React, {Component, Fragment} from 'react';

import ExpansionPanel from '@material-ui/core/ExpansionPanel';
import ExpansionPanelSummary from '@material-ui/core/ExpansionPanelSummary';
import ExpansionPanelDetails from '@material-ui/core/ExpansionPanelDetails';
import ExpandMoreIcon from '@material-ui/icons/ExpandMore';
import Typography from '@material-ui/core/Typography';

import ConfigField from './config-field';


class ConfigPanel extends Component {

    constructor() {
        super();
        this.renderConfigPanel = this.renderConfigPanel.bind(this);
    }

    renderConfigPanel(key) {
        const config = this.props.config[key];
        const changes = this.props.changes[key] || {};

        return <ExpansionPanel key={ key }>
            <ExpansionPanelSummary
              expandIcon={<ExpandMoreIcon />}
              aria-controls="panel1a-content"
              id={ `config_${ key }` }
            >
                <Typography className="capitalize">{ this.props.label || key }</Typography>
            </ExpansionPanelSummary>
            <ExpansionPanelDetails className="settings-form">
                <ConfigField 
                    config={ config } 
                    changes={ changes }
                    configKey={ key }
                    onChange={ this.props.onChange }
                    disabled={ this.props.disabled }
                />
            </ExpansionPanelDetails>
        </ExpansionPanel>;
    }

    render() {
        if (!this.props.config) {
            return null;
        }
        return <Fragment>{
            Object.keys(this.props.config).map(this.renderConfigPanel)
        }</Fragment>;
    }
}

export default ConfigPanel;