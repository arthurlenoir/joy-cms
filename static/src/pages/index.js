import React, {Component, Fragment} from 'react';
import update from 'immutability-helper';

import Button from '@material-ui/core/Button';
import CircularProgress from '@material-ui/core/CircularProgress'
import Grid from '@material-ui/core/Grid';
import MenuList from '@material-ui/core/MenuList';
import MenuItem from '@material-ui/core/MenuItem';
import Typography from '@material-ui/core/Typography';

import axios from 'axios';

import ConfigPanel from './config-panel';
import Editor from './editor';
import HTMLPreview from './html-preview';


const flattenReducer = (res, item) => {
    if (typeof item === "object") {
        const items = Object.values(item);
        if (items.length > 0) {
            return [
            ...items.reduce(flattenReducer, res)
            ];
        }
    }
    else {
        return [
            ...res,
            item
        ]
    }
    return res;
 };


class Pages extends Component {
    constructor() {
        super();
        this.state = {
            appsList: [],
            apps: {},
            currentChanges: {},
            currentPage: null,
            currentApp: null,
            saving: {}
        }
        this.updateCurrentPage = this.updateCurrentPage.bind(this);
        this.submitSettings = this.submitSettings.bind(this);
        this.onConfigChange = this.onConfigChange.bind(this);
        this.onAppConfigChange = this.onAppConfigChange.bind(this);
        this.renderApp = this.renderApp.bind(this);
    }

    componentWillMount() {
        axios.get('/admin/api/apps/')
        .then(r => this.setState({
            appsList: r.data.map(app => app.id),
            apps: r.data.reduce((res, app) => ({
                [app.id]: {
                    ...app,
                    pagesList: app.pages.map(page => page.file),
                    pages: app.pages.reduce((res, page) => ({
                        [page.file]: page
                    }), {})
                }
            }), {})
        }));
    }

    displayPage(app, page) {
        console.log("displayPage", app, page);
        this.setState({
            currentApp: app.id,
            currentPage: page.file
        });
    }

    updateCurrentPage(template_data) {
        const currentAppId = this.state.currentApp;
        const currentPageFile = this.state.currentPage;
        this.setState(state => {
            const currentAppChanges = state.currentChanges[currentAppId] || {};
            const currentChanges = currentAppChanges[currentPageFile] || {};
            const currentApp = state.apps[currentAppId];
            const currentPage = currentApp.pages[currentPageFile];
            if (currentPage.template_data === template_data) {
                const res = {
                    ...currentChanges
                };
                delete res.template_data;
                return {
                    currentChanges: {
                        [currentAppId]: {
                            ...currentAppChanges,
                            [currentPageFile]: res
                        }
                    }
                };
            }
            return {
                currentChanges: {
                    [currentAppId]: {
                        ...currentAppChanges,
                        [currentPageFile]: {
                            ...currentChanges,
                            template_data
                        }
                    }
                }
            };
        });
    }

    updatePage(pages, page, currentChanges) {
        return {
            ...pages,
            [page.file]: update(page, Object.keys(currentChanges).reduce((res, key) => {
                const changes = currentChanges[key];
                console.log(key, changes);
                if (typeof changes === "object" && changes !== null && !Array.isArray(changes)) {
                    return {
                        ...res,
                        [key]: Object.keys(changes).reduce((res, key) => {
                            return {
                                ...res,
                                [key]: {$merge: changes[key]}
                            }
                        }, {})
                    }
                }
                return {
                    ...res,
                    $merge: {
                        ...(res.$merge || {}),
                        [key]: changes
                    }
                }
            }, {}))
        }
    }

    savePage(event, page) {
        event.stopPropagation();
        const currentAppId = this.state.currentApp;
        const currentPageFile = this.state.currentPage;
        this.setState(state => {
            const currentAppChanges = state.currentChanges[currentAppId] || {};
            const currentChanges = currentAppChanges.pages[currentPageFile] || {};
            const currentApp = state.apps[currentAppId];
            const currentPage = currentApp.pages[currentPageFile];
            const pages = this.updatePage(currentApp.pages, currentPage, currentChanges);

            axios.put(`/admin/api/apps/${ encodeURIComponent(currentAppId) }/pages/${ encodeURIComponent(encodeURIComponent(currentPageFile)) }`, currentChanges)
            .then(r => {
                this.setState(state => {
                    const currentApp = state.apps[currentAppId];
                    const currentPage = currentApp.pages[currentPageFile];
                    const pages = this.updatePage(currentApp.pages, currentPage, r.data);
                    return {
                        saving: {
                            ...state.saving,
                            [currentAppId]: {
                                ...(state.saving[currentAppId] || {}),
                                pages: {
                                    ...((state.saving[currentAppId] && state.saving[currentAppId].pages) || {}),
                                    [currentPageFile]: false
                                }
                            }
                        },
                        apps: {
                            ...state.apps,
                            [currentAppId]: {
                                ...currentApp,
                                pages
                            }
                        }
                    }
                });
            })
            .catch(e => console.log("error", e, e.response));

            return {
                apps: {
                    ...state.apps,
                    [currentAppId]: {
                        ...currentApp,
                        pages
                    }
                },
                currentChanges: {
                    ...state.currentChanges,
                    [currentAppId]: {
                        ...(state.currentChanges[currentAppId] || {}),
                        pages: {
                            [currentPageFile]: {}
                        }
                    }
                },
                saving: {
                    ...state.saving,
                    [currentAppId]: {
                        ...(state.saving[currentAppId] || {}),
                        pages: {
                             ...((state.saving[currentAppId] && state.saving[currentAppId].pages) || {}),
                            [currentPageFile]: true
                        }
                    }
                }
            };
        });
    }

    saveAppConfig(event, app) {
        event.stopPropagation();
        this.setState(state => {
            const currentApp = state.apps[app.id];
            const currentAppChanges = state.currentChanges[app.id] || {};
            const config = {
                ...currentApp.config,
                ...currentAppChanges.config
            }
            axios.put(`/admin/api/apps/${ encodeURIComponent(app.id) }/`, {config: currentAppChanges.config})
            .then(r => {
                this.setState(state => {
                    return {
                        saving: {
                            ...state.saving,
                            [app.id]: {
                                ...(state.saving[app.id] || {}),
                                config: false
                            }
                        }
                    }
                });
            });
            return {
                apps: {
                    ...state.apps,
                    [app.id]: {
                        ...currentApp,
                        config: config
                    }
                },
                currentChanges: {
                    ...state.currentChanges,
                    [app.id]: {
                        ...(state.currentChanges[app.id] || {}),
                        config: {}
                    }
                },
                saving: {
                    ...state.saving,
                    [app.id]: {
                        ...(state.saving[app.id] || {}),
                        config: true
                    }
                }
            };
        });

    }

    submitSettings(event) {
        event.preventDefault();
    }

    getCurrentApp() {
        if (this.state.currentApp) {
            return this.state.apps[this.state.currentApp];
        }
        return null;
    }

    getCurrentPage() {
        if (this.state.currentPage) {
            const app = this.getCurrentApp();
            if (app) {
                return app.pages[this.state.currentPage];
            }
        }
        return null;
    }

    onAppConfigChange(updateQuery) {
        const currentAppId = this.state.currentApp;
        updateQuery = Object.values(updateQuery)[0];
        this.setState(state => {
            const currentAppChanges = state.currentChanges[currentAppId] || {};
            return {
                currentChanges: {
                    ...state.currentChanges,
                    [currentAppId]: {
                        ...currentAppChanges,
                        config: {
                            ...(currentAppChanges.config || {}),
                            ...updateQuery
                        }
                    }
                }
            };
        });

    }

    onConfigChange(updateQuery) {
        const currentAppId = this.state.currentApp;
        const currentPageFile = this.state.currentPage;
        this.setState(state => {
            const currentAppChanges = state.currentChanges[currentAppId] || {pages: {}};
            const currentChanges = currentAppChanges.pages[currentPageFile] || {};
            const configChanges = currentChanges.config || {};
            const currentApp = state.apps[currentAppId];
            const currentPage = currentApp.pages[currentPageFile];
            return {
                currentChanges: {
                    ...state.currentChanges,
                    [currentAppId]: {
                        ...currentAppChanges,
                        pages: {
                            [currentPageFile]: {
                                ...currentChanges,
                                config: {
                                    ...configChanges,
                                    ...Object.keys(updateQuery).reduce((res, key) => {
                                        const currentConfig = currentPage.config[key];
                                        const updateSubQuery = updateQuery[key]
                                        return {
                                            ...res,
                                            [key]: {
                                                ...Object.keys(updateSubQuery).reduce((res, key) => {
                                                    if (updateSubQuery[key] === currentConfig[key]) {
                                                        res = {...res};
                                                        delete res[key];
                                                        return res;
                                                    }
                                                    return {
                                                        ...res,
                                                        [key]: updateSubQuery[key]
                                                    }
                                                }, res[key])
                                            }
                                        };
                                    }, configChanges)
                                }
                            }
                        }
                    }
                }
            };
        });
    }

    getCurrentChanges() {
        const currentAppChanges = this.state.currentChanges[this.state.currentApp];
        if (currentAppChanges && currentAppChanges.pages) {
            return currentAppChanges.pages[this.state.currentPage] || null;
        }
        return null;
    }

    currentAppHasChanges() {
        const currentAppChanges = this.state.currentChanges[this.state.currentApp];
        return currentAppChanges && 
            typeof currentAppChanges.config === "object" &&
            Object.keys(currentAppChanges.config).length > 0;

    }

    currentPageHasChanges() {
        const currentAppChanges = this.state.currentChanges[this.state.currentApp];
        if (!currentAppChanges || !currentAppChanges.pages) {
            return false;
        }
        const currentChanges = currentAppChanges.pages[this.state.currentPage];
        if (!currentChanges) {
            return false;
        }
        console.log("currentChanges", currentChanges);
        return typeof currentChanges === "object" && 
                Object.keys(currentChanges).length > 0 &&
                Object.values(currentChanges).reduce(flattenReducer, []).length > 0;
    }

    renderConfigForm(currentPage) {
        if (!currentPage || !currentPage.config) {
            return null;
        }

        const changes = this.getCurrentChanges() || {};
        const currentAppSaving = this.state.saving[this.state.currentApp];

        return <ConfigPanel 
            config={ currentPage.config }
            changes={ changes.config || {} }
            onChange={ this.onConfigChange }
            disabled={ currentAppSaving && currentAppSaving[this.state.currentPage] === true }
        />;
    }

    renderApp(appId) {
        const app = this.state.apps[appId];
        const appSaving = this.state.saving[appId] || {};
        const pagesSaving = appSaving.pages || {};

        return <Fragment key={appId}>
            <div className="page-list-app-title">
                <Typography variant="h6">{ app.config.name || app.id }</Typography>

                { appSaving.config &&
                    <div>
                        Saving...
                        <CircularProgress
                            className="btn-spinner"
                            size={20}
                        />
                    </div>
                ||
                    <Button 
                        variant="contained" 
                        color="primary"
                        className="page-list-item-savebtn not-loading"
                        disabled={!this.currentAppHasChanges()}
                        onClick={ e => this.saveAppConfig(e, app) }
                    >Save</Button>
                }
            </div>
            <MenuList>
                { 
                    app.pagesList.map(pageFile => {
                        const page = app.pages[pageFile];
                        return <MenuItem 
                            key={ `${ app.id }/${ page.file }` }
                            onClick={() => this.displayPage(app, page) }
                            className="page-list-item"
                            selected={ this.state.currentApp === app.id && this.state.currentPage === page.file }
                        >
                            <div>
                                <div>{ page.config.settings.title }</div>
                                <div className="page-list-url">url: { page.url }</div>
                            </div>
                            { pagesSaving[page.file] &&
                                <div>
                                    Saving...
                                    <CircularProgress
                                        className="btn-spinner"
                                        size={20}
                                    />
                                </div>
                            ||
                                <Button 
                                    variant="contained" 
                                    color="primary"
                                    className="page-list-item-savebtn not-loading"
                                    disabled={!this.currentPageHasChanges()}
                                    onClick={ e => this.savePage(e, page) }
                                >Save</Button>
                            }
                        </MenuItem>;
                    }) 
                }
            </MenuList>
        </Fragment>
    }

    render() {
        const currentApp = this.getCurrentApp();
        const currentPage = this.getCurrentPage();

        return <Fragment>
            <Grid item xs={2} className="main-layout-column">
                { this.state.appsList.map(this.renderApp) }
            </Grid>
            <Grid item xs={3} className="main-layout-column">
                { currentApp && <ConfigPanel 
                        config={{config: currentApp.config}}
                        label={`${ currentApp.config.name || currentApp.id } Settings`}
                        changes={ this.state.currentChanges[currentApp.id] || {} }
                        onChange={ this.onAppConfigChange }
                        disabled={ this.state.saving[this.state.currentApp] && this.state.saving[this.state.currentApp].config && Object.values(this.state.saving[this.state.currentApp].config).length > 0 }
                    />
                }
                { this.renderConfigForm(currentPage) }
            </Grid>
            <Grid item xs={5}>
                { currentPage && 
                    <Editor 
                        value={ currentPage.template_data } 
                        errors={ currentPage.errors }
                        onChange={ this.updateCurrentPage }
                        disabled={ this.state.saving[this.state.currentPage] === true }
                    />
                }
                { currentPage && currentPage.html && 
                    <HTMLPreview html={ currentPage.html } />
                }
            </Grid>
        </Fragment>;
    }
}

export default Pages;