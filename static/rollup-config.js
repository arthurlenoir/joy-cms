// this is the rollup plugin that adds babel as a compilation stage.
import babel from 'rollup-plugin-babel';

//Convert CommonJS modules to ES6, 
// so they can be included in a Rollup bundle
import commonjs from 'rollup-plugin-commonjs'

// Rollup plugin to minify generated bundle.
import { uglify } from 'rollup-plugin-uglify'
import { terser } from "rollup-plugin-terser";


// Replace strings in files while bundling them.
import replace from 'rollup-plugin-replace'

import resolve from 'rollup-plugin-node-resolve'

// Serve your rolled up bundle like webpack-dev-server
// without hot reload
import serve from 'rollup-plugin-serve'

// this will refresh the browser when detect changes in bundle.
import livereload from 'rollup-plugin-livereload'

import json from 'rollup-plugin-json';

// this will create index.html file dynamically 
// with the script tag pointing to bundle.
//import htmlTemplate from 'rollup-plugin-generate-html-template';

// this will insert the styles into style tag in html
import autoprefixer from 'autoprefixer'
import postcss from 'rollup-plugin-postcss';
import scss from 'rollup-plugin-scss';

var globals = {
  'react': 'React',
  'react-dom': 'ReactDOM',
  'react-codemirror': 'CodeMirror'
}

var namedExports = {
  './node_modules/react/react.js': [ 
    'cloneElement', 'createElement', 'PropTypes', 
    'Children', 'Component', 'Fragment', 'isValidElement'
  ],
  './node_modules/react/index.js': [ 
    'cloneElement', 'createElement', 'PropTypes', 
    'Children', 'Component', 'Fragment', 'isValidElement'
  ],
  './node_modules/react-dom/index.js': ['findDOMNode', 'render'],
  './node_modules/react-is/index.js': ['isForwardRef', 'isValidElementType', 'ForwardRef'],
  './node_modules/@material-ui/core/styles/index.js': ['makeStyles'],
  './node_modules/prop-types/index.js': ['bool', 'element', 'elementType', 'func', 'oneOfType']
}
 
var prodConfig = {
  input:  './src/index.js',
  output:  { 
    file: './dist/bundle.prod.js',
    format: 'iife',
    globals: globals
  },
  plugins: [
    postcss({
      extensions: [ '.css' ],
    }),
    resolve({
       mainFields: ['module', 'main'],
       browser: true
    }),
    babel({
      exclude: 'node_modules/**',
      presets: ['@babel/preset-react', '@babel/preset-env']
    }),
    commonjs({
      include: 'node_modules/**',
      exclude: [
        'node_modules/process-es6/**'
      ],
      namedExports: namedExports
    }),
    replace({
      'process.env.NODE_ENV': JSON.stringify( 'production' )
    }),
    uglify({
      output: {
        comments: false
      },
      sourcemap: false
    })
  ]
}
 
var developmentConfig = {
  input:  './src/index.js',
  output:  { 
    file: './dist/bundle.js',
    format: 'iife',
    globals: globals
  },
  plugins: [
    postcss({
      extensions: [ '.css' ],
    }),
    json({
      // All JSON files will be parsed by default,
      // but you can also specifically include/exclude files
      include: 'node_modules/**',

      // for tree-shaking, properties will be declared as
      // variables, using either `var` or `const`
      preferConst: true, // Default: false

      // specify indentation for the generated default export —
      // defaults to '\t'
      indent: '  ',

      // ignores indent and generates the smallest code
      compact: true, // Default: false

      // generate a named export for every property of the JSON object
      namedExports: true // Default: true
    }),
    resolve({
       mainFields: ['module', 'main'],
       browser: true
    }),
    babel({
      exclude: 'node_modules/**',
      presets: ['@babel/preset-react']
    }),
    commonjs({
      include: 'node_modules/**',
      exclude: [
        'node_modules/process-es6/**'
      ],
      namedExports: namedExports
    }),
    replace({
      'process.env.NODE_ENV': JSON.stringify( 'development' )
    }),
    serve({
      contentBase: 'dist',
      open: true
    }),
    livereload({
      watch: 'dist'
    })
  ]
}
 
export default developmentConfig;
