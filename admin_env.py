from jinja2 import Environment, FileSystemLoader, select_autoescape


admin_env = Environment(
    loader=FileSystemLoader('templates'),
    autoescape=select_autoescape(['html', 'xml'])
)

admin_env.globals['STATIC_PREFIX'] = '/admin/static/'
