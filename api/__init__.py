from sanic.response import json
from sanic import Blueprint

from .apps import apps
from .pages import pages
from .themes import themes

api = Blueprint.group(apps, pages, themes, url_prefix="/admin/api")
