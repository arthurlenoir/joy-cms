from aiofile import AIOFile

from copy import deepcopy

from sanic.response import json
from sanic import Blueprint

from urllib.parse import unquote

from yaml import dump

from env import env
from utils import merge


pages = Blueprint("pages", url_prefix="/pages")


@pages.route('/')
async def get_list(request):
    templates = []
    for url, template in env.loader.urls.items():
        templates.append(template.to_dict(env))
    return json(templates)

async def write_config(config, filepath):
    data = dump(config)
    async with AIOFile(filepath, 'w') as afp:
        await afp.write(data)
        await afp.fsync()

async def write_template(template_data, filepath):
    async with AIOFile(filepath, 'w') as afp:
        await afp.write(template_data)
        await afp.fsync()

@pages.route('/<path>', methods=['POST', 'PUT'])
async def post(request, path):
    path = unquote(path)
    template = env.loader.get_from_path(path)
    if template is None:
        return json({"file not found"})

    template_copy = deepcopy(template)

    mustSaveConfig = False
    mustSaveTemplate = False

    config_update = request.json.get('config')
    if config_update is not None:
        merge(config_update, template_copy.config)
        mustSaveConfig = True

    if 'template_data' in request.json:
        template_copy.template_data = request.json['template_data']
        mustSaveTemplate = True

    res = await template_copy.to_dict_with_preview(env)

    if 'errors' not in res:
        if mustSaveConfig:
            await write_config(template_copy.config, template_copy.config_file)
        if mustSaveTemplate:
            await write_template(template_copy.template_data, template_copy.template_file)

    return json(res)
