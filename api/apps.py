from aiofile import AIOFile

from sanic import Blueprint
from sanic.exceptions import NotFound
from sanic.response import json

from copy import deepcopy

from urllib.parse import unquote
from yaml import dump

from env import env
from utils import merge


apps = Blueprint("apps", url_prefix="/apps")


@apps.route('/')
async def get_list(request):
    return json([app.to_dict(env) for app in env.loader.apps.values()])


def get_app(app_id):
    app = env.loader.apps.get(app_id)
    
    if app is None:
        raise NotFound("App '%s' does not exist." % app_id)

    return app


@apps.route('/<app_id:[A-z0-9]+>/', methods=['POST', 'PUT'])
async def update_app(request, app_id):
    app = get_app(app_id)
    if 'config' in request.json:
        merge(request.json['config'], app.config)
        await write_config(app.config, app.settings_file)
    return json(app.to_dict(env))



@apps.route('/<app_id:[A-z0-9]+>/pages/')
async def get_page_list(request, app_id):
    app = get_app(app_id)
    return json(app.to_dict(env).get('pages', []))


async def write_config(config, filepath):
    data = dump(config)
    async with AIOFile(filepath, 'w') as afp:
        await afp.write(data)
        await afp.fsync()


async def write_template(template_data, filepath):
    async with AIOFile(filepath, 'w') as afp:
        await afp.write(template_data)
        await afp.fsync()


@apps.route('/<app_id:[A-z0-9]+>/pages/<path>', methods=['POST', 'PUT'])
async def update_page(request, app_id, path):
    app = get_app(app_id)
    path = unquote(path)

    template = app.get_from_path(path)
    if template is None:
        raise NotFound("Template '%s' does not exist." % path)

    template_copy = deepcopy(template)

    mustSaveConfig = False
    mustSaveTemplate = False

    config_update = request.json.get('config')
    if config_update is not None:
        merge(config_update, template_copy.config)
        mustSaveConfig = True

    if 'template_data' in request.json:
        template_copy.template_data = request.json['template_data']
        mustSaveTemplate = True

    res = await template_copy.to_dict_with_preview(env)

    if 'errors' not in res:
        if mustSaveConfig:
            await write_config(template_copy.config, template_copy.config_file)
        if mustSaveTemplate:
            await write_template(template_copy.template_data, template_copy.template_file)

    return json(res)