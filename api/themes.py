from sanic import Blueprint
from sanic.response import json

from env import env

themes = Blueprint("themes", url_prefix="/themes")


@themes.route('/')
async def get_list(request):
    return json([theme.to_dict(env) for theme in env.loader.themes.values()])


@themes.route('/<theme_id:[A-z0-9]+>/pages/')
async def get_page_list(request, theme_id):
    theme = env.loader.themes.get(theme_id)
    
    if theme is None:
        raise NotFound("Theme '%s' does not exist." % theme_id)

    return json(theme.to_dict(env).get('pages', []))