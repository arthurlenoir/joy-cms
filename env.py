from jinja2 import Environment, FileSystemLoader, select_autoescape
from loader import TemplateLoader
from os import path

themes_directory = path.join(path.dirname(path.abspath(__file__)), 'themes')
apps_directory = path.join(path.dirname(path.abspath(__file__)), 'apps')

env = Environment(
    loader=TemplateLoader(themes_directory=themes_directory, apps_directory=apps_directory),
    autoescape=select_autoescape(['html', 'xml']),
    enable_async=True
)