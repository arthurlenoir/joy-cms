from sanic import Sanic
from sanic.response import html, json

from admin_env import admin_env
from api import api
from env import env


app = Sanic()

app.static('/admin/static', './static/dist')

app.blueprint(api)


@app.route("/admin")
async def admin(request):
    return html(admin_env.get_template("main.html").render())


@app.listener('before_server_start')
async def init(sanic, loop):
    await env.loader.load_all_templates(app, env)


if __name__ == "__main__":
    app.run(host="0.0.0.0", port=8000)